package V8;

import java.util.Arrays;
import java.text.DecimalFormat;
import java.util.Scanner;

public class T1{
	public static void main(String[] args){
		BottleDispenser b = BottleDispenser.getInstance();
		int val;
		Scanner read = new Scanner(System.in);
		
		do{
			System.out.println("");
			System.out.println("*** LIMSA-AUTOMAATTI ***");
			System.out.println("1) Lisää rahaa koneeseen");
			System.out.println("2) Osta pullo");
			System.out.println("3) Ota rahat ulos");
			System.out.println("4) Listaa koneessa olevat pullot");
			System.out.println("0) Lopeta");
			System.out.print("Valintasi: ");
			val = read.nextInt();
			
			switch(val){
				case 1: b.addMoney(); break;
				case 2: {
					b.printBottles();
					System.out.print("Valintasi: ");
					val = read.nextInt();
					b.buyBottle(val);
					break;
				}
				case 3: b.returnMoney(); break;
				case 4: b.printBottles(); break;
			}
		}while(val != 0);
		
		read.close();
	}
	
	private static class BottleDispenser {
	    
	    private int count;
	    private float money;
		private Bottle[] bottles;
		
		static private BottleDispenser dp = null;
	    
	    private BottleDispenser() {
	        count = 6;
	        money = 0;
			bottles = new Bottle[count];
	        bottles[0] = new Bottle();
			bottles[1] = new Bottle("Pepsi Max", 1.5f, 2.2f);
			bottles[2] = new Bottle("Coca-Cola Zero", 0.5f, 2.0f);
			bottles[3] = new Bottle("Coca-Cola Zero", 1.5f, 2.5f);
			bottles[4] = new Bottle("Fanta Zero", 0.5f, 1.95f);
			bottles[5] = new Bottle("Fanta Zero", 0.5f, 1.95f);
	    }
	    
	    static public BottleDispenser getInstance() {
	    	if(dp == null)
	    		dp = new BottleDispenser();
	    	return dp;
	    }
	    
	    public void addMoney() {
	        money += 1;
	        System.out.println("Klink! Lisää rahaa laitteeseen!");
	    }
	    
	    public void buyBottle(int p) {
			int id = p-1;
			if(count > 0 && money > bottles[id].price){
				money -= bottles[id].price;
	    	    System.out.println("KACHUNK! "+bottles[id].name+" tipahti masiinasta!");
				deleteBottle(id);
			} else {
				System.out.println("Syötä rahaa ensin!");
			}
		}
		
		public void deleteBottle(int id){
			count -= 1;
			if(id<count-1)
				bottles = concat(Arrays.copyOfRange(bottles, 0, id),
							 Arrays.copyOfRange(bottles, id+1, bottles.length));
			else
				bottles = Arrays.copyOf(bottles, count);
		}
	    
	    public void returnMoney() {
			DecimalFormat df = new DecimalFormat("0.00");
	        System.out.println("Klink klink. Sinne menivät rahat! Rahaa tuli ulos "
							   + df.format(money).replace('.',',') + "€");
			money = 0;
	    }
		
		public void printBottles(){
			int p;
			for(int i = 0; i<bottles.length; i++){
				p = i+1;
				System.out.println(p + ". Nimi: " + bottles[i].name);
				System.out.println("\tKoko: " + bottles[i].vol
								  + "\tHinta: " + bottles[i].price);
			}
		}
		
		public Bottle[] concat(Bottle[] a, Bottle[] b) {
	   		int aLen = a.length;
	   		int bLen = b.length;
	   		Bottle[] c = new Bottle[aLen+bLen];
	   		System.arraycopy(a, 0, c, 0, aLen);
	   		System.arraycopy(b, 0, c, aLen, bLen);
	   		return c;
		}
		
		private class Bottle{
			public final String name;
			public final float vol;
			public final float price;
			
			public Bottle(){
				this("Pepsi Max", 0.5f, 1.8f);
			}
			public Bottle(String nm, float v, float p){
				name = nm;
				vol = v;
				price = p;
			}
		}
	}
}
