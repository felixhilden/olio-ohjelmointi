package V8;

import java.text.DecimalFormat;
import java.util.ArrayList;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class T2 extends Application{
	private static BottleDispenser b;
	@FXML
	private Label info;
	
	public static void main(String[] args){
		b = BottleDispenser.getInstance();
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent layout = FXMLLoader.load(getClass().getResource("V8T2.fxml"));
		Scene scene = new Scene(layout);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public void addMoneyHandle(ActionEvent event) {
		info.setText(b.addMoney());
	}
	
	public void retMoneyHandle(ActionEvent event) {
		info.setText(b.returnMoney());
	}
	
	public void buyBottleHandle(ActionEvent event) {
		info.setText(b.buyBottle(0));
	}
	
	private static class BottleDispenser {
	    
	    private float money;
		private ArrayList<Bottle> bottles;
		
		static private BottleDispenser dp = null;
	    
	    private BottleDispenser() {
	        money = 0;
	        bottles = new ArrayList<Bottle>();
	        bottles.add(new Bottle("Pepsi Max", 0.5f, 1.8f));
	        bottles.add(new Bottle("Pepsi Max", 1.5f, 2.2f));
	        bottles.add(new Bottle("Coca-Cola Zero", 0.5f, 2.0f));
	        bottles.add(new Bottle("Coca-Cola Zero", 1.5f, 2.5f));
	        bottles.add(new Bottle("Fanta Zero", 0.5f, 1.95f));
	        bottles.add(new Bottle("Fanta Zero", 0.5f, 1.95f));
	    }
	    
	    static public BottleDispenser getInstance() {
	    	if(dp == null)
	    		dp = new BottleDispenser();
	    	return dp;
	    }
	    
	    public String addMoney() {
	        money += 1;
	        return "Klink! Lisää rahaa laitteeseen! " + money;
	    }
	    
	    public String buyBottle(int id) {
			if(bottles.size() > 0 && money > bottles.get(id).price){
				money -= bottles.get(id).price;
				bottles.remove(id);
	    	    return "KACHUNK! "+bottles.get(id).name+" tipahti masiinasta!";
			} else {
				return "Syötä rahaa ensin!";
			}
		}
	    
	    public String returnMoney() {
			DecimalFormat df = new DecimalFormat("0.00");
	        String ret = "Klink klink. Sinne menivät rahat! Rahaa tuli ulos "
							   + df.format(money).replace('.',',') + "€";
			money = 0;
			return ret;
	    }
		
		private class Bottle{
			public final String name;
			public final float price;
			
			public Bottle(String nm, float v, float p){
				name = nm;
				price = p;
			}
		}
	}
}
