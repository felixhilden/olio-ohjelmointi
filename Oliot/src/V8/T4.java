package V8;

import java.text.DecimalFormat;
import java.util.ArrayList;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.stage.Stage;

public class T4 extends Application{
	private static BottleDispenser b;
	@FXML
	private Label info;
	@FXML
	private Slider amount;
	@FXML
	private ChoiceBox<String> dropdown = new ChoiceBox<>();
	
	public static void main(String[] args){
		b = BottleDispenser.getInstance();
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent layout = FXMLLoader.load(getClass().getResource("V8T4.fxml"));
		Scene scene = new Scene(layout);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	@FXML
	private void initialize() {
		dropdown.setItems(b.getItems());
	}
	
	public void addMoneyHandle(ActionEvent event) {
		info.setText(b.addMoney(amount.getValue()));
		amount.setValue(0.0);
	}
	
	public void retMoneyHandle(ActionEvent event) {
		info.setText(b.returnMoney());
	}
	
	public void buyBottleHandle(ActionEvent event) {
		info.setText(b.buyBottle(dropdown.getItems().indexOf(dropdown.getValue())));
	}
	
	private static class BottleDispenser {
	    
	    private double money;
		private ArrayList<Bottle> bottles;
		
		static private BottleDispenser dp = null;
		DecimalFormat df = new DecimalFormat("0.00");
	    
	    private BottleDispenser() {
	        money = 0;
	        bottles = new ArrayList<Bottle>();
	        bottles.add(new Bottle("Pepsi Max", 0.5f, 1.8f, 5));
	        bottles.add(new Bottle("Pepsi Max", 1.5f, 2.2f, 5));
	        bottles.add(new Bottle("Coca-Cola Zero", 0.5f, 2.0f, 5));
	        bottles.add(new Bottle("Coca-Cola Zero", 1.5f, 2.5f, 5));
	        bottles.add(new Bottle("Fanta Zero", 0.5f, 1.95f, 5));
	    }
	    
	    static public BottleDispenser getInstance() {
	    	if(dp == null)
	    		dp = new BottleDispenser();
	    	return dp;
	    }
	    
	    public ObservableList<String> getItems(){
	    	ObservableList<String> namelist = FXCollections.observableArrayList();
	    	for(Bottle bot : bottles) {
	    		namelist.add(bot.name + ": " + df.format(bot.vol) + " l, "
	    				+ df.format(bot.price) + " €");
	    	}
	    	return namelist;
	    }
	    
	    public String addMoney(double amount) {
	        money += amount;
	        return "Klink! Total: " + df.format(money) + " €";
	    }
	    
	    public String buyBottle(int id) {
	    	if(bottles.get(id).count == 0)
	    		return "No bottles left!";
			if(money >= bottles.get(id).price){
				money -= bottles.get(id).price;
				bottles.get(id).count -= 1;
	    	    return "KACHUNK! "+bottles.get(id).name+" rolled out!";
			} else {
				return "Add money first!";
			}
		}
	    
	    public String returnMoney() {
	        String ret = "Klink klink. There goes the money! You got "
							   + df.format(money) + " €";
			money = 0;
			return ret;
	    }
		
		private class Bottle{
			public final String name;
			public final float vol;
			public final float price;
			public int count;
			
			public Bottle(String nm, float v, float p, int c){
				name = nm;
				vol = v;
				price = p;
				count = c;
			}
		}
	}
}
