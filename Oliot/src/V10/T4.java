package V10;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class T4 extends Application implements Initializable{
	@FXML
	private Button loadBtn;
	@FXML
	private TextField addressBar;
	@FXML
	private WebView wview;
	
	private String prev = "";
	private String next = "";
	
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		wview.getEngine().load("http://www.lut.fi");
	}
	
	@Override
	public void start(Stage primaryStage) {
		Parent layout = null;
		try {
			layout = FXMLLoader.load(getClass().getResource("T4.fxml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene(layout);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public void go(ActionEvent e) {
		prev = wview.getEngine().getLocation();
		String address = addressBar.getText();
		
		if(address.equals("index.html")) {
			wview.getEngine().load(getClass().getResource("index.html").toExternalForm());
		} else {
			if(!address.startsWith("http://"))
				address = "http://" + address;
			wview.getEngine().load(address);
		}
	}
	
	public void refresh(ActionEvent e) {
		String address = wview.getEngine().getLocation();
		wview.getEngine().load(address);
	}
	
	public void push(ActionEvent e) {
		wview.getEngine().executeScript("document.shoutOut()");
	}
	
	public void reset(ActionEvent e) {
		wview.getEngine().executeScript("initialize()");
	}
	
	public void previous(ActionEvent e) {
		if(!prev.isEmpty()) {
			next = wview.getEngine().getLocation();
			wview.getEngine().load(prev);
			prev = "";
		}
	}
	
	public void next(ActionEvent e) {
		if(!next.isEmpty()) {
			prev = wview.getEngine().getLocation();
			wview.getEngine().load(next);
			next = "";
		}
	}
}
