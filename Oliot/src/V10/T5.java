package V10;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class T5 extends Application implements Initializable{
	@FXML
	private Button loadBtn;
	@FXML
	private TextField addressBar;
	@FXML
	private WebView wview;
	
	private ArrayList<String> prev = new ArrayList<>(10);
	private ArrayList<String> next = new ArrayList<>(10);
	
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		wview.getEngine().load("http://www.lut.fi");
	}
	
	@Override
	public void start(Stage primaryStage) {
		Parent layout = null;
		try {
			layout = FXMLLoader.load(getClass().getResource("T5.fxml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene(layout);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public void go(ActionEvent e) {
		append(prev, wview.getEngine().getLocation());
		String address = addressBar.getText();
		
		if(address.equals("index.html")) {
			wview.getEngine().load(getClass().getResource("index.html").toExternalForm());
		} else {
			if(!address.startsWith("http://"))
				address = "http://" + address;
			wview.getEngine().load(address);
		}
	}
	
	public void refresh(ActionEvent e) {
		String address = wview.getEngine().getLocation();
		wview.getEngine().load(address);
	}
	
	public void push(ActionEvent e) {
		wview.getEngine().executeScript("document.shoutOut()");
	}
	
	public void reset(ActionEvent e) {
		wview.getEngine().executeScript("initialize()");
	}
	
	public void previous(ActionEvent e) {
		if(!prev.isEmpty()) {
			append(next, wview.getEngine().getLocation());
			wview.getEngine().load(pop(prev));
		}
	}
	
	public void next(ActionEvent e) {
		if(!next.isEmpty()) {
			append(prev, wview.getEngine().getLocation());
			wview.getEngine().load(pop(next));
		}
	}
	
	private static void append(ArrayList<String> list, String item) {
		if(list.size() < 10)
			list.add(item);
		else {
			Collections.rotate(list, 9);
			list.remove(9);
			list.add(item);
		}
	}
	
	private static String pop(ArrayList<String> list) {
		if(list.size() > 0) {
			String ret = list.get(list.size()-1);
			list.remove(list.size()-1);
			return ret;
		} else {
			return "";
		}
		
	}
}
