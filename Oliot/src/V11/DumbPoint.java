package V11;

import javafx.scene.shape.Circle;

public class DumbPoint {

	private Circle c;
	
	public DumbPoint(double x, double y, int rad, T4 parent) {
		c = new Circle(x, y, rad);
		c.setOnMouseClicked(e -> {
			shout();
			parent.click(x, y);
			e.consume();
        });
	}

	public static void shout() {
		System.out.println("Hei, olen piste!");
	}
	
	public Circle get() {
		return c;
	}
}
