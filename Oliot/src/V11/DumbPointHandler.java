package V11;

import java.util.ArrayList;

public class DumbPointHandler {
	private static DumbPointHandler inst = new DumbPointHandler();
	private ArrayList<DumbPoint> points;
	
	private DumbPointHandler() {
		points = new ArrayList<>();
	}
	
	public int psize() {
		return points.size();
	}
	
	public DumbPoint getPoint(int id) {
		return points.get(id);
	}
	
	public void addPoint(DumbPoint p) {
		points.add(p);
	}
	
	public void addPoint(double x, double y, int rad, T4 parent) {
		points.add(new DumbPoint(x, y, rad, parent));
	}
	
	public void rmPoint(int id) {
		points.remove(id);
	}
	
	static public DumbPointHandler getInstance() {
    	return inst;
    }
}
