package V11;

import javafx.scene.shape.Circle;

public class Point {

	private Circle c;
	
	public Point(double x, double y, int rad) {
		c = new Circle(x, y, rad);
		c.setOnMouseClicked(e -> {
			shout();
			e.consume();
        });
	}
	
	public static void shout() {
		System.out.println("Hei, olen piste!");
	}
	
	public Circle get() {
		return c;
	}
}
