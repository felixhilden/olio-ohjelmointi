package V11;

import java.util.ArrayList;

import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;

public class ShapeHandler {
	private static ShapeHandler inst = new ShapeHandler();
	private AnchorPane pane = null;
	private ArrayList<SmartPoint> points;
	private ArrayList<Line> lines;
	
	private double x1 = -1;
	private double y1 = 0;
	
	private ShapeHandler() {
		points = new ArrayList<>();
		lines = new ArrayList<>();
	}
	
	public void setPane(AnchorPane p) {
		pane = p;
	}
	
	public void addPoint(SmartPoint p) {
		points.add(p);
		pane.getChildren().add(p.get());
		x1 = -1;
	}
	
	public void addPoint(double x, double y, int rad) {
		SmartPoint p = new SmartPoint(x, y, rad, this);
		points.add(p);
		pane.getChildren().add(p.get());
		x1 = -1;
	}
	
	public void click(double x, double y) {
		if(x1 == -1) {
			x1 = x;
			y1 = y;
		} else {
			Line line = new Line(x1, y1, x, y);
			x1 = -1;
			lines.add(line);
			pane.getChildren().add(line);
		}
	}
	
	static public ShapeHandler getInstance() {
    	return inst;
    }
}