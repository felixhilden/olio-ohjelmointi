package V11;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

public class T4 extends Application{
	@FXML
	private AnchorPane pane;
	
	private static DumbPointHandler points;
	
	/* Single line */
	private double x1 = -1;
	private double y1 = 0;
	private int index = 0;

	public static void main(String[] args) {
		points = DumbPointHandler.getInstance();
		launch(args);
	}
	
	
	@Override
	public void start(Stage primaryStage) {
		Parent layout = null;
		try {
			layout = FXMLLoader.load(getClass().getResource("T4.fxml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		layout.setStyle("-fx-background-image:url('"
				+ getClass().getResource("Suomen-kartta.jpg").toExternalForm() + "')");
		Scene scene = new Scene(layout);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public void onClick(MouseEvent e) {
		DumbPoint p = new DumbPoint(e.getSceneX(), e.getSceneY(), 2, this);
		points.addPoint(p);
		pane.getChildren().add(p.get());
	}
	
	public void click(double x, double y) {
		if(x1 == -1) {
			x1 = x;
			y1 = y;
		} else {
			if(index != 0) {
				pane.getChildren().remove(index);
				index = 0;
			}
			Line line = new Line(x1, y1, x, y);
			pane.getChildren().add(line);
			index = pane.getChildren().size() - 1;
			x1 = -1;
		}
	}
}
