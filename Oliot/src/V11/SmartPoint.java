package V11;

import javafx.scene.shape.Circle;

public class SmartPoint {

	private Circle c;
	
	public SmartPoint(double x, double y, int rad, ShapeHandler sh) {
		c = new Circle(x, y, rad);
		c.setOnMouseClicked(e -> {
			shout();
			sh.click(x, y);
			e.consume();
        });
	}
	
	public static void shout() {
		System.out.println("Hei, olen piste!");
	}
	
	public Circle get() {
		return c;
	}
}
