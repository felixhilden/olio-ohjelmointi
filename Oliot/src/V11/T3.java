package V11;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class T3 extends Application{
	@FXML
	private AnchorPane pane;
	
	private static PointHandler points;

	public static void main(String[] args) {
		points = PointHandler.getInstance();
		launch(args);
	}
	
	
	@Override
	public void start(Stage primaryStage) {
		Parent layout = null;
		try {
			layout = FXMLLoader.load(getClass().getResource("T3.fxml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		layout.setStyle("-fx-background-image:url('"
				+ getClass().getResource("Suomen-kartta.jpg").toExternalForm() + "')");
		Scene scene = new Scene(layout);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public void onClick(MouseEvent e) {
		Point p = new Point(e.getSceneX(), e.getSceneY(), 2);
		points.addPoint(p);
		pane.getChildren().add(p.get());
	}
}
