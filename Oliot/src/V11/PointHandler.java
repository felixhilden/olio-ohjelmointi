package V11;

import java.util.ArrayList;

public class PointHandler {
	private static PointHandler inst = new PointHandler();
	private ArrayList<Point> points;
	
	private PointHandler() {
		points = new ArrayList<>();
	}
	
	public int psize() {
		return points.size();
	}
	
	public Point getPoint(int id) {
		return points.get(id);
	}
	
	public void addPoint(Point p) {
		points.add(p);
	}
	
	public void addPoint(double x, double y, int rad) {
		points.add(new Point(x, y, rad));
	}
	
	public void rmPoint(int id) {
		points.remove(id);
	}
	
	static public PointHandler getInstance() {
    	return inst;
    }
}
