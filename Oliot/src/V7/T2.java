package V7;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class T2 extends Application{
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("T2");
		
		Label label = new Label("Change me!");
		
		Button button = new Button();
		button.setText("Say hello");
		button.setOnAction(e -> label.setText("Hello World!"));
		
		VBox layout = new VBox(20);
		layout.getChildren().addAll(label, button);
		
		Scene scene = new Scene(layout, 500, 300);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
