package V7;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.geometry.Pos;

public class T5 extends Application{
	
	String filename = "T5default.txt";
	String content;
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("T5");
		
		Label lb = new Label("-filename-");
		TextField tf = new TextField("Write here");
		Button load = new Button("Load");
		Button save = new Button("Save");
		
		load.setOnAction(e -> {
			GetTextBox.display("Load file", "Filename:", this);
			lb.setText(filename);

			File in = new File(filename);
			try {
				Scanner sc = new Scanner(in);
				tf.setText("");
				while(sc.hasNext()) {
					tf.setText(tf.getText() + sc.next() + " ");
				}
				sc.close();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
		});
		save.setOnAction(e -> {
			GetTextBox.display("Save file", "Filename:", this);
			lb.setText(filename);
			
			File out = new File(filename);
			try {
				FileWriter fw = new FileWriter(out);
				PrintWriter pw = new PrintWriter(fw);
				
				pw.print(tf.getText());
				pw.close();
				
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
		});
		
		VBox layout = new VBox(20);
		layout.getChildren().addAll(tf, lb, load, save);
		
		Scene scene = new Scene(layout, 500, 300);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public void setFilename(String name){
		this.filename = name;
	}
	public String getFilename(){
		return this.filename;
	}
}

class GetTextBox {
	public static void display(String title, String message, T5 apk) {
		Stage window = new Stage();
		
		window.initModality(Modality.APPLICATION_MODAL);
		window.setTitle(title);
		window.setMinWidth(250);
		
		Label label = new Label();
		label.setText(message);
		TextField tf = new TextField(apk.getFilename());
		
		Button button1 = new Button("OK");
		Button button2 = new Button("Cancel");
		
		button1.setOnAction(e -> {
			apk.setFilename(tf.getText());
			window.close();
		});
		button2.setOnAction(e -> {
			window.close();
		});
		
		VBox layout = new VBox(10);
		layout.getChildren().addAll(label, tf, button1, button2);
		layout.setAlignment(Pos.CENTER);
		
		Scene scene = new Scene(layout);
		window.setScene(scene);
		window.showAndWait();
	}
}
