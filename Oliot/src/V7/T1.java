package V7;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class T1 extends Application{

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("T1");
		Button button = new Button();
		button.setText("Say hello");
		
		button.setOnAction(e -> System.out.println("Hello World!"));
		
		StackPane layout = new StackPane();
		layout.getChildren().add(button);
		
		Scene scene = new Scene(layout, 500, 300);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
