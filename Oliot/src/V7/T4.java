package V7;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class T4 extends Application{
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("T4");
		
		Label label = new Label("Enter to change text");
		TextField tf = new TextField("Write here");
		
		tf.setOnAction(e -> label.setText(tf.getText()));
		
		VBox layout = new VBox(20);
		layout.getChildren().addAll(label, tf);
		
		Scene scene = new Scene(layout, 500, 300);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
