package smartpost;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class PackageBuilder implements Initializable{
	/* Package class radio buttons */
	@FXML private ToggleGroup group;
	@FXML private RadioButton radio1;
	@FXML private RadioButton radio2;
	@FXML private RadioButton radio3;
	/* Preset packages */
	@FXML private ChoiceBox<String> dropdown = new ChoiceBox<>();
	/* Custom packages */
	@FXML private TextField namefield;
	@FXML private TextField weightfield;
	@FXML private TextField sizefield;
	@FXML private CheckBox fragfield;

	private SmartPostNetwork spn = SmartPostNetwork.getInstance();
	private ArrayList<Package> presets = new ArrayList<>(6);
	private ObservableList<String> dropdownItems;
	
	/* Package class restrictions */
	public static final float maxWeight = 50;
	public static final float maxSize1 = 2000;
	public static final float maxSize2 = 500;
	public static final float maxSize3 = 2000;
	
	public PackageBuilder() {}
	public PackageBuilder(Stage owner) {
		fstart(owner);
	}
	
	/** Create window */
	private void fstart(Stage owner) {
		Stage window = new Stage();
		window.initModality(Modality.APPLICATION_MODAL);
		window.setTitle("Uusi paketti");
		FXMLLoader loader;
		Scene scene;
		try {
			loader = new FXMLLoader(getClass().getResource("PackageBuilder.fxml"));
			scene = new Scene(loader.load());
			scene.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());
			window.initOwner(owner);
			window.setScene(scene);
			window.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	@Override /* Set dropdown list items */
	public void initialize(URL location, ResourceBundle resources) {
		createPresets();
		ArrayList<String> names = new ArrayList<>(7);
		names.add("- Valitse esine -");
		for(int i = 0; i < presets.size(); i++) {
			names.add(presets.get(i).name);
		}
		dropdownItems = FXCollections.observableArrayList(names);
		dropdown.setItems(dropdownItems);
		dropdown.setValue(dropdownItems.get(0));
	}
	
	/** Read input: generate package and exit or show error message */
	@FXML private void newPackage(ActionEvent e) {
		/* Read input fields */
		String name = namefield.getText();
		double weight;
		double size;
		boolean fragile = fragfield.isSelected();;
		
		try {
			weight = Double.parseDouble(weightfield.getText());
			size = Double.parseDouble(sizefield.getText());
		} catch (NumberFormatException ex) {
			weight = 0;
			size = 0;
		}
		
		if(name.isEmpty() && weight == 0 && size == 0) {
			/* If no user input, create from preset */
			int preID = dropdown.getItems().indexOf(dropdown.getValue());
			if(preID == 0) {
				/* Default item not selectable */
				AlertBox.display("Valitse paketti!", "Pakettia ei ole valittu!");
				return;
			}
			Package pre = presets.get(preID-1);
			name = pre.name;
			weight = pre.weight;
			size = pre.size;
			fragile = pre.fragile;
		} else if(!name.isEmpty() && weight > 0 && size > 0){
			/* If valid user input, do nothing */
		} else {
			/* Invalid user input */
			AlertBox.display("Väärä syöte!", "Paketin tiedot eivät kelpaa!");
			return;
		}

		/* Build package if able, if not, display relevant alert box and return */
		Package p = null;
		if(weight > maxWeight) {
			AlertBox.display("Paketti on liian painava!", "Paketin maksimipaino on 50 kg!");
			return;
		}
		if(radio1.isSelected()) {
			/* Class 1 package */
			if(size > maxSize1) {
				AlertBox.display("Paketti on liian suuri!", "Luokan 1 paketin maksimikoko on 2000 litraa!");
				return;
			}
			p = new C1Package(1, name, weight, size, fragile);
		} else if(radio2.isSelected()) {
			/* Class 2 package */
			if(size > maxSize2) {
				AlertBox.display("Paketti on liian suuri!", "Luokan 2 paketin maksimikoko on 500 litraa!");
				return;
			}
			p = new C2Package(2, name, weight, size, fragile);
		} else if(radio3.isSelected()) {
			/* Class 3 package */
			if(size > maxSize3) {
				AlertBox.display("Paketti on liian suuri!", "Luokan 1 paketin maksimikoko on 2000 litraa!");
				return;
			}
			p = new C3Package(3, name, weight, size, fragile);
		}
		
		spn.addToCentral(p);
		AlertBox.display("Paketti luotu!", "Uusi paketti luotiin ja lähetettiin keskusvarastoon onnistuneesti!");
		onClose(e);
	}
	
	/** Close window */
	@FXML private void onClose(ActionEvent e) {
		Node source = (Node)e.getSource();
		Stage win = (Stage)source.getScene().getWindow();
		win.close();
	}
	
	/** Create preset items to choose from */
	private void createPresets() {
		C2Package p1 = new C2Package(2, "Täytetty ilves", 18.4, 600.0, true);
		C2Package p2 = new C2Package(2, "Jätkänkynttilä", 13.0, 27, false);
		C2Package p3 = new C2Package(2, "Kristallimaljakko", 2.8, 6.0, true);
		C2Package p4 = new C2Package(2, "50 litraa.. vettä", 50.0, 50.0, true);
		C2Package p5 = new C2Package(2, "Pistin talvisodasta", 0.8, 0.7, false);
		C2Package p6 = new C2Package(2, "Vinyylilevykokoelma", 6.3, 20.0, true);
		presets.add(p1);
		presets.add(p2);
		presets.add(p3);
		presets.add(p4);
		presets.add(p5);
		presets.add(p6);
	}
}
