package smartpost;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class GUIController extends Application implements Initializable{
	@FXML private WebView webview;
	/* Search */
	@FXML private TextField citysrc;
	@FXML private TextField addrsrc;
	@FXML private TextField address;
	@FXML private TextField radius;
	@FXML private ListView<String> searched;
	/* Storage */
	@FXML private Label storageparent;
	@FXML private Label storagelbl;
	@FXML private ListView<String> storage;
	/* Map, sender and receiver */
	@FXML private Label receiver;
	
	private Stage owner;
	private SmartPostNetwork spn = SmartPostNetwork.getInstance();
	private ArrayList<SmartPostNode> nodes = new ArrayList<>();
	private SmartPostNode sndnode = spn.getCentral();
	private SmartPostNode rcvnode = null;

	public GUIController() {}
	/** Main GUI responsible for managing user input,<br>
	 * spawns another window with PackageBuilder */
	public GUIController(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) {
		Parent layout = null;
		owner = primaryStage;
		try {
			layout = FXMLLoader.load(getClass().getResource("GUI.fxml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene(layout);
		scene.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		/* Load map */
		webview.getEngine().load(getClass().getResource("index.html").toExternalForm());
		
		/* Create global context menu */
		ContextMenu menu = new ContextMenu();
		MenuItem i1 = new MenuItem("Tallenna varastojen tila");
		MenuItem i2 = new MenuItem("Tyhjennä keskusvarasto");
		MenuItem i3 = new MenuItem("Tyhjennä muut varastot");
		MenuItem i4 = new MenuItem("Tyhjennä kaikki");
		
		/* Click actions */
		i1.setOnAction(e -> {
			/* Export data to file */
			Serializer.serialize(spn.getNodes());
			Serializer.serialize(spn.getCentral());
			Logger.Log("-- Tiedot tallennettiin tiedostoihin --");
			AlertBox.display("Tiedot tallennettu", "Varastojen nykyinen tila on tallennettu tiedostoon");
		});
		i2.setOnAction(e -> {
			spn.initCentral();
			Logger.Log("-- Keskusvarasto tyhjennettiin --");
			AlertBox.display("Keskusvarasto tyhjennetty!", "Keskusvarasto on nyt tyhjä!");
			updateStorage();
		});
		i3.setOnAction(e -> {
			spn.initNodes();
			Logger.Log("-- Muut varastot tyhjennettiin --");
			AlertBox.display("Varastot tyhjennetty!", "Muut varastot ovat nyt tyhjiä!");
			updateStorage();
		});
		i4.setOnAction(e -> {
			spn.initCentral();
			spn.initNodes();
			Logger.Log("-- Kaikki varastot tyhjennettiin --");
			AlertBox.display("Varastot tyhjennetty!", "Kaikki varastot ovat nyt tyhjiä!");
			updateStorage();
		});
		
		/* Create menu */
		menu.getItems().addAll(i1, new SeparatorMenuItem(), i2, i3, new SeparatorMenuItem(), i4);
		storageparent.setContextMenu(menu);
		storageparent.setOnContextMenuRequested(e -> menu.show(storageparent, e.getScreenX(), e.getScreenY()));
	}
	
	/** Set ListView items after search */
	private void setSearched() {
		if(nodes.size() > 0) {
			/* Items found, update list */
			ArrayList<String> names = new ArrayList<>(nodes.size());
			for(SmartPostNode node : nodes) {
				names.add(node.getName());
			}
			searched.setItems(FXCollections.observableArrayList(names));
			searched.getSelectionModel().select(0);
			/* Clear search */
			citysrc.setText("");
			addrsrc.setText("");
			address.setText("");
			radius.setText("");
		} else {
			/* No items found */
			AlertBox.display("Ei tuloksia!", "Haku ei löytänyt tuloksia annetuilla ehdoilla.");
		}
	}
	
	/** Clear other fields */
	@FXML private void onWriteCity(KeyEvent e) {
		addrsrc.setText("");
		address.setText("");
		radius.setText("");
	}
	
	/** Clear other fields */
	@FXML private void onWriteAddress(KeyEvent e) {
		citysrc.setText("");
		address.setText("");
		radius.setText("");
	}
	
	/** Clear other fields (not radius) */
	@FXML private void onWriteFullAddr(KeyEvent e) {
		addrsrc.setText("");
		citysrc.setText("");
	}
	
	/** Clear other fields (not adjacent address) */
	@FXML private void onWriteRadius(KeyEvent e) {
		addrsrc.setText("");
		citysrc.setText("");
	}
	
	/** Search with input parameters and set nodes list accordingly */
	@FXML private void onSearch(ActionEvent e) {
		String search;
		if(!(search = citysrc.getText()).isEmpty()) {
			/* Search by city name */
			nodes = spn.getByCity(search.toUpperCase());
			setSearched();
		} else if(!(search = addrsrc.getText()).isEmpty()) {
			/* Search by address */
			nodes = spn.getByAddress(search.toUpperCase());
			setSearched();
		} else if(!(search = address.getText()).isEmpty()) {
			/* Search by address and radius */
			
			// Get XML data from Google Maps API
			search = search.replace(" ", "+");
			String url = "http://maps.google.com/maps/api/geocode/xml?address="+search;
			String content = URLReader.read(url);
			
			// Full address
			ArrayList<String> children = new ArrayList<>(1);
			children.add("formatted_address");
			ArrayList<ArrayList<String>> elems = XMLParser.get(content, "result", children);
			address.setText(elems.get(0).get(0));
			
			// Coordinates
			children = new ArrayList<>(2);
			children.add("lat");
			children.add("lng");
			elems = XMLParser.get(content, "location", children);

			double lat = Double.parseDouble(elems.get(0).get(0));
			double lng = Double.parseDouble(elems.get(0).get(1));
			double rad;
			try {
				rad = Double.parseDouble(radius.getText());
			} catch (NumberFormatException ex) {
				rad = 0;
				radius.setText("0");
			}
			
			nodes = spn.getByRadius(lat, lng, rad*1000);
			setSearched();
		}
	}
	
	/** Show storage of selected SmartPostNode */
	@FXML private void onShowStorage(ActionEvent e) {
		if(nodes.size() > 0) {
			/* Search not empty, update storage */
			sndnode = getSelectedSearch();
			storagelbl.setText(sndnode.getName());
			updateStorage();
		} else {
			/* Search empty */
			AlertBox.display("Ei valintaa!", "Valitse automaatti näyttääksesi varaston.");
		}
	}
	
	/** Open a new window to create new package */
	@FXML private void onNewPackage(ActionEvent e) {
		new PackageBuilder(owner);
	}
	
	/** Send package from current storage to selected receiver */
	@FXML private void onSendPackage(ActionEvent e) {
		if(rcvnode == null) {
			/* No receiver selected */
			AlertBox.display("Ei määränpäätä!", "Aseta ensin määränpää lähetykselle!");
			return;
		}
		if(sndnode.getStorage().size() > 0) {
			/* Items in storage */
			String sendnode = storagelbl.getText();
			int id = storage.getSelectionModel().getSelectedIndex();
			
			if(sendnode.equals("Keskusvarasto")) {
				/* From central storage, don't draw path */
				spn.sendFromCentral(id, rcvnode);
			} else {
				/* From ordinary node, draw path, update storage */
				int pclass = spn.sendPackage(id, sndnode, rcvnode);
				if(pclass > 0) {
					webview.getEngine().executeScript("document.createPath([\""
							+ Double.toString(sndnode.latitude) + "\", \""
							+ Double.toString(sndnode.longitude) + "\", \""
							+ Double.toString(rcvnode.latitude) + "\", \""
							+ Double.toString(rcvnode.longitude) + "\"], \"red\", "
							+ Integer.toString(pclass) + ")");
				} else {
					return;
				}
			}
			updateStorage();
			AlertBox.display("Lähetys toimitettu!", "Paketti lähetettiin ja vastaanotettiin!");
		} else {
			/* No items in storage */
			AlertBox.display("Varasto tyhjä!", "Varasto on tyhjä tai sitä ei ole valittu!");
		}
	}
	
	/** Show central storage */
	@FXML private void onShowCentral(ActionEvent e) {
		sndnode = spn.getCentral();
		storagelbl.setText("Keskusvarasto");
		updateStorage();
	}
	
	/** Search for nodes whose storages are not empty */
	@FXML private void onShowNonempty(ActionEvent e) {
		nodes = spn.getNonempty();
		if(nodes.size() > 0) {
			setSearched();
		} else {
			searched.setItems(FXCollections.observableArrayList());
			AlertBox.display("Ei tuloksia!", "Kaikkien automaattien varastot ovat tyhjillään.");
		}
	}
	
	/** Show last selected node on map */
	@FXML private void onAddSelected(ActionEvent e) {
		if(nodes.size() > 0) {
			/* Search contains nodes */
			SmartPostNode node = getSelectedSearch();
			webview.getEngine().executeScript("document.goToLocation(\"" + 
			node.address + ", " + node.code + " " + node.city + "\", \"" + 
					node.office + ", " + node.availability + "\", \"red\")");
		} else {
			/* No nodes */
			AlertBox.display("Ei valintaa!", "Valitse automaatti lisätäksesi kartalle.");
		}
	}
	
	/** Show all search results on map */
	@FXML private void onAddSearched(ActionEvent e) {
		if(nodes.size() > 0) {
			/* Search contains nodes */
			for(SmartPostNode node : nodes) {
				webview.getEngine().executeScript("document.goToLocation(\"" + 
				node.address + ", " + node.code + " " + node.city + "\", \"" + 
						node.office + ", " + node.availability + "\", \"red\")");
			}
		} else {
			/* No nodes */
			AlertBox.display("Ei automaatteja!", "Hae automaatteja lisätäksesi kartalle.");
		}
		
	}
	
	/** Clear created paths between nodes */
	@FXML private void onClearPaths(ActionEvent e) {
		webview.getEngine().executeScript("document.deletePaths()");
	}

	/** Reload page: clear paths and marked nodes */
	@FXML private void onClearMap(ActionEvent e) {
		webview.getEngine().load(getClass().getResource("index.html").toExternalForm());
	}
	
	/** Set receiver node */
	@FXML private void onToReceiver(ActionEvent e) {
		if(nodes.size() > 0) {
			rcvnode = getSelectedSearch();
			receiver.setText(rcvnode.getName());
		} else {
			AlertBox.display("Ei valintaa!", "Valitse automaatti asettaaksesi vastaanottajan.");
		}
	}
	
	/** Get node pointed to by current selection */
	private SmartPostNode getSelectedSearch() {
		return nodes.get(searched.getSelectionModel().getSelectedIndex());
	}
	
	/** Update storage list and select first item */
	private void updateStorage() {
		storage.setItems(FXCollections.observableArrayList(sndnode.getStorage()));
		storage.getSelectionModel().select(0);
	}
}
