package smartpost;

import java.util.ArrayList;

public class Mainclass {
	/** Start TIMO */
	public static void main(String[] args) {
		/* Import previous data if able */
		SmartPostNetwork spn = SmartPostNetwork.getInstance();
		ArrayList<SmartPostNode> nodes = Serializer.deserNodes();
		SmartPostNode central = Serializer.deserCentral();
		
		if(nodes != null)
			spn.setNodes(nodes);
		if(central != null)
			spn.setCentral(central);
		
		Logger.open();
		Logger.Log("---- Käynnistys ----");
		
		/* Start program (and wait for close) */
		new GUIController(args);
		
		Logger.close();
	}
}
