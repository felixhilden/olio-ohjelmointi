package smartpost;

public class C1Package extends Package{
	private static final long serialVersionUID = 3179297292438889779L;

	public C1Package(int cl, String nm, double w, double s, boolean fr) {
		super(cl, nm, w, s, fr);
	}

	@Override
	public void move() {
		if(fragile)
			broken = true;
	}

}
