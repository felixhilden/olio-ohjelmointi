package smartpost;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class Serializer {
	/** Deserialize SmartPostNode list inside SPNodes.ser file */
	@SuppressWarnings("unchecked")
	public static ArrayList<SmartPostNode> deserNodes(){
		FileInputStream in;
		try {
			in = new FileInputStream("SPNodes.ser");
			ObjectInputStream obin = new ObjectInputStream(in);
			ArrayList<SmartPostNode> nodes = (ArrayList<SmartPostNode>)obin.readObject();
			obin.close();
			in.close();
			return nodes;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			/* FILE NOT FOUND */
		}
		return null;
	}
	
	/** Deserialize SmartPostNode inside Central.ser file */
	public static SmartPostNode deserCentral(){
		FileInputStream in;
		try {
			in = new FileInputStream("Central.ser");
			ObjectInputStream obin = new ObjectInputStream(in);
			SmartPostNode node = (SmartPostNode)obin.readObject();
			obin.close();
			in.close();
			return node;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			/* FILE NOT FOUND */
		}
		return null;
	}
	
	/** Serialize SmartPostNode list to SPNodes.ser */
	public static void serialize(ArrayList<SmartPostNode> nodes) {
		try {
			FileOutputStream out = new FileOutputStream("SPNodes.ser");
			ObjectOutputStream obout = new ObjectOutputStream(out);
			obout.writeObject(nodes);
			obout.close();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** Serialize SmartPostNode to Central.ser */
	public static void serialize(SmartPostNode node) {
		try {
			FileOutputStream out = new FileOutputStream("Central.ser");
			ObjectOutputStream obout = new ObjectOutputStream(out);
			obout.writeObject(node);
			obout.close();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
