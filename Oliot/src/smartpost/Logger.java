package smartpost;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Logger {
	private static final String mainfile = "mainlog.txt";
	private static final String tempfile = "templog.txt";
	
	private static File main;
	private static File temp;
	private static FileWriter mainfw;
	private static FileWriter tempfw;
	private static PrintWriter mainpw;
	private static PrintWriter temppw;
	
	private Logger() {}
	
	/** Open streams */
	public static void open() {
		/* Open a main log file */
		main = new File(mainfile);
		if(!main.exists()) {
			try {
				main.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
		/* Create an empty temporary file */
		temp = new File(tempfile);
		if(temp.exists() && temp.isFile())
			temp.delete();
		try {
			temp.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		/* Create writers, append to main log */
		try {
			mainfw = new FileWriter(main, true);
			tempfw = new FileWriter(temp);
			mainpw = new PrintWriter(mainfw);
			temppw = new PrintWriter(tempfw);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** Close streams */
	public static void close() {
		mainpw.close();
		temppw.close();
		try {
			mainfw.close();
			tempfw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** Print to both logs */
	public static void Log(String msg) {
		mainpw.println(msg);
		temppw.println(msg);
	}
}
