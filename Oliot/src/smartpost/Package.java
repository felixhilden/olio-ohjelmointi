package smartpost;

import java.io.Serializable;
import java.text.DecimalFormat;

public abstract class Package implements Serializable{
	private static final long serialVersionUID = -3686340744096978176L;
	/** Package class for labeling */
	public final int pclass;
	public final String name;
	/** Weight as kilograms  */
	public final double weight;
	/** Size as liters (dm^3) */
	public final double size;
	public final boolean fragile;
	protected boolean broken = false;
	
	public Package(int cl, String nm, double w, double s, boolean fr) {
		pclass = cl;
		name = nm;
		weight = w;
		size= s;
		fragile = fr;
	}
	
	/** Determines whether something happens to the package when moved */
	abstract public void move();
	
	/** Return package string representation */
	public String getName() {
		DecimalFormat df = new DecimalFormat("#.##");
		if(broken) {
			return "RIKKI! L" + pclass + ": " + name + ", " + df.format(weight)
			+ " kg, " + df.format(size) + " l";
		} else {
			return "L" + pclass + ": " + name + ", " + df.format(weight)
			+ " kg, " + df.format(size) + " l";
		}
	}
}
