package smartpost;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class URLReader {
	/** Read content from given url address */
	public static String read(String s) {
		URL url;
		String content = "";
		try {
			url = new URL(s);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
			return "";
		}
		BufferedReader br;
		try {
			br = new BufferedReader(new InputStreamReader(url.openStream()));
			/* Read line by line and append */
			String line = "";
			while((line = br.readLine()) != null) {
				content += line + '\n';
			}
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
		return content;
	}
}
