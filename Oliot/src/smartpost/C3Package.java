package smartpost;

import java.util.Random;

public class C3Package extends Package{
	private static final long serialVersionUID = -7766092586811319780L;

	public C3Package(int cl, String nm, double w, double s, boolean fr) {
		super(cl, nm, w, s, fr);
	}

	@Override
	public void move() {
		/* Normalize to P(10kg 1m^3 will break) = 0.5 */
		double w = (double) weight;
		double s = (double) size;
		double prop = Math.pow(2, -w/20 - s/2000);
		Random rand = new Random();
		/* Break item by chance */
		if(rand.nextDouble() < prop && fragile)
			broken = true;
	}

}
