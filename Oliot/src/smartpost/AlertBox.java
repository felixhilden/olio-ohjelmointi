package smartpost;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AlertBox {
	/** 
	 * Display a popup with title, message and an OK button.<br>
	 * Prevent interacting with other windows and wait for close.
	 */
	public static void display(String title, String msg) {
		Stage win = new Stage();
		win.initModality(Modality.APPLICATION_MODAL);
		win.setTitle(title);
		win.setMinWidth(350);
		win.setMinHeight(150);
		
		Label label = new Label();
		label.setWrapText(true);
		label.setText(msg);
		
		Button close = new Button("OK");
		close.setOnAction(e -> win.close());
		
		VBox layout = new VBox(10);
		layout.setAlignment(Pos.CENTER);
		layout.getChildren().addAll(label, close);
		
		Scene scene = new Scene(layout);
		win.setScene(scene);
		win.showAndWait();
	}
}
