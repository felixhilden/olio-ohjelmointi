package smartpost;

import java.io.Serializable;
import java.util.ArrayList;

public class SmartPostNode implements Serializable{
	private static final long serialVersionUID = 653801956320112319L;
	public final String code;
	public final String city;
	public final String address;
	public final String availability;
	public final String office;
	public final double latitude;
	public final double longitude;
	
	private ArrayList<Package> storage = new ArrayList<>();

	/** Directly assign attributes */
	public SmartPostNode(String cd, String ct, String ad,
			String av, String of, double lat, double lon) {
		code = cd;
		city = ct;
		address = ad;
		availability = av;
		office = of;
		latitude = lat;
		longitude = lon;
	}
	
	/** Parse attributes from ArrayList */
	public SmartPostNode(ArrayList<String> attr) throws ArrayIndexOutOfBoundsException {
		if(attr.size() != 7) {
			throw new ArrayIndexOutOfBoundsException(
					"Invalid amount of attributes given to SmartPostNode");
		} else {
			code = attr.get(0);
			city = attr.get(1);
			address = attr.get(2);
			availability = attr.get(3);
			office = attr.get(4);
			latitude = Double.parseDouble(attr.get(5));
			longitude = Double.parseDouble(attr.get(6));
		}
	}
	
	/** Add package to storage */
	public void add(Package p) {
		storage.add(p);
	}
	
	/** Remove and return package from storage */
	public Package pop(int id) {
		Package p = storage.get(id);
		storage.remove(id);
		return p;
	}
	
	/** Remove package from storage */
	public void remove(int id) {
		storage.remove(id);
	}

	/** Get storage string representations */
	public ArrayList<String> getStorage() {
		ArrayList<String> names = new ArrayList<>(storage.size());
		
		for(Package p : storage)
			names.add(p.getName());
			
		return names;
	}
	
	/** Get node string representation */
	public String getName(){
		return office + ", " + address + ", " + city;
	}
}
