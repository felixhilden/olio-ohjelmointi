package smartpost;

public class C2Package extends Package{
	private static final long serialVersionUID = 7419516656707202466L;

	public C2Package(int cl, String nm, double w, double s, boolean fr) {
		super(cl, nm, w, s, fr);
	}

	@Override
	public void move() {
		// Class 2 doesn't break when moved
	}

}
