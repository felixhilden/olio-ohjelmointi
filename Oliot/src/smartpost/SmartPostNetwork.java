package smartpost;

import java.util.ArrayList;

/**
 * SmartPostNetwork singleton<br>
 * &emsp;&emsp;Parse XML information from preset URL to a list of SmartPostNodes.
 * <br><br>
 * getInstance(): SmartPostNetwork<br>
 * &emsp;&emsp;Return instance of SPN.
 */
public class SmartPostNetwork{
	private static SmartPostNetwork spn = new SmartPostNetwork();
	private ArrayList<SmartPostNode> nodes = null;
	private SmartPostNode centralStorage = null;
	
	private SmartPostNetwork() {
		initNodes();
		initCentral();
	}
	
	/** Initialize or reset SmartPostNodes in the network */
	public void initNodes() {
		String url = "http://smartpost.ee/fi_apt.xml";
		String content = URLReader.read(url);
		
		ArrayList<String> children = new ArrayList<>(7);
		children.add("code");
		children.add("city");
		children.add("address");
		children.add("availability");
		children.add("postoffice");
		children.add("lat");
		children.add("lng");
		
		ArrayList<ArrayList<String>> elems = XMLParser.get(content, "place", children);
		nodes = new ArrayList<>(elems.size());
		for(int i = 0; i < elems.size(); i++) {
			nodes.add(new SmartPostNode(elems.get(i)));
		}		
	}
	
	/** Initialize or reset the central storage of the network */
	public void initCentral() {
		centralStorage = new SmartPostNode("", "", "", "", "", 0, 0);
	}
	
	/** Add package to the central storage */
	public void addToCentral(Package p) {
		centralStorage.add(p);
		Logger.Log("Keskusvarastoon lisättiin " + p.getName());
	}
	
	/** Send package from central storage to ordinary node */
	public void sendFromCentral(int id, SmartPostNode to) {
		if(id < centralStorage.getStorage().size()) {
			Package p = centralStorage.pop(id);
			Logger.Log("LÄHETYS: Keskusvarasto - " + to.getName());
			Logger.Log("\t" + p.getName());
			nodes.get(nodes.indexOf(to)).add(p);
		}	
	}
	
	/** Send package from a node to another, call Package.move() */
	public int sendPackage(int id, SmartPostNode from, SmartPostNode to) {
		if(id < from.getStorage().size()) {
			Package p = from.pop(id);
			if(p.pclass == 1 && GCS.distance(from.latitude, from.longitude,
					to.latitude, to.longitude) > 150000) {
				AlertBox.display("Liian pitkä matka!", "Luokan 1 paketin maksimikuljetusmatka on 150 km");
				from.add(p);
				return 0;
			}
			Logger.Log("LÄHETYS: " + from.getName() + " - " + to.getName());
			Logger.Log("\t" + p.getName());
			p.move();
			Logger.Log("\t-> " + p.getName());
			to.add(p);
			return p.pclass;
		}
		return 0;
	}
	
	/** Search nodes by city name */
	public ArrayList<SmartPostNode> getByCity(String s) {
		ArrayList<SmartPostNode> found = new ArrayList<>();
		for(SmartPostNode node : nodes) {
			if(node.city.contains(s))
				found.add(node);
		}
		return found;
	}
	
	/** Search nodes by address */
	public ArrayList<SmartPostNode> getByAddress(String s) {
		ArrayList<SmartPostNode> found = new ArrayList<>();
		for(SmartPostNode node : nodes) {
			if(node.address.toUpperCase().contains(s))
				found.add(node);
		}
		return found;
	}
	
	/** Search nodes by given address and a radius around it */
	public ArrayList<SmartPostNode> getByRadius(double lat, double lng, double m) {
		ArrayList<SmartPostNode> found = new ArrayList<>();
		for(SmartPostNode node : nodes) {
			double lat2 = node.latitude;
			double lng2 = node.longitude;
			if(GCS.distance(lat, lng, lat2, lng2) < m)
				found.add(node);
		}
		return found;
	}
	
	/** Search for nodes whose storage is not empty */
	public ArrayList<SmartPostNode> getNonempty(){
		ArrayList<SmartPostNode> found = new ArrayList<>();
		for(SmartPostNode node : nodes) {
			if(node.getStorage().size() != 0)
				found.add(node);
		}
		return found;
	}
	
	public ArrayList<SmartPostNode> getNodes() {
		return nodes;
	}
	
	public SmartPostNode getCentral() {
		return centralStorage;
	}
	
	public static SmartPostNetwork getInstance() {
		return spn;
	}
	
	public void setNodes(ArrayList<SmartPostNode> nds) {
		nodes = nds;
	}
	
	public void setCentral(SmartPostNode nd) {
		centralStorage = nd;
	}
}

/** Global coordinate system distances calculator */
class GCS {
	/** Earth's radius */
	static final long R = 6371000;
	
	/** Distance between two global coordinates in meters using Haversine formula */
	public static double distance(double lat1, double lng1, double lat2, double lng2) {
		double phi1 = toRad(lat1);
		double phi2 = toRad(lat2);
		double lmb1 = toRad(lng1);
		double lmb2 = toRad(lng2);
		
		double dp = phi2-phi1;
		double dl = lmb2-lmb1;
		
		double a = Math.sin(dp/2)*Math.sin(dp) +
				Math.cos(phi1)*Math.cos(phi2)*Math.sin(dl/2)*Math.sin(dl/2);
		double c = 2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		return R * c;
	}
	
	/** Angles to radians */
	private static double toRad(double ang) {
		return Math.PI*ang / 180;
	}
}
