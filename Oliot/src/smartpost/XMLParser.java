package smartpost;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLParser {	
	/** Parse content given in XML form:<br>
	 * &emsp;&emsp;Get nodes named parent and their children and<br>
	 * &emsp;&emsp;read text content of children into 2D ArrayList
	 */
	public static ArrayList<ArrayList<String>> get(String content, String parent, ArrayList<String> children) {		
		Document doc;
		ArrayList<ArrayList<String>> elems = new ArrayList<>();
		/* Parse content into NodeList */
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dBuilder = dbf.newDocumentBuilder();
			
			doc = dBuilder.parse(new InputSource(new StringReader(content)));
			doc.getDocumentElement().normalize();
			
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
			return new ArrayList<ArrayList<String>>();
		}
		
		/* Make 2D ArrayList of requested tags */
		NodeList nodes = doc.getElementsByTagName(parent);
		
		/* Figure out attribute id's for nodes */
		int[] ids = new int[children.size()];
		for(int i = 0; i < ids.length; i++)
			ids[i] = -1;
		
		int loops = 0;
		do {
			Node node = nodes.item(loops);
			if(node.getNodeType() == Node.ELEMENT_NODE) {
				Element elem = (Element) node;
				NodeList attr = elem.getChildNodes();
				
				/* For every child element */
				for(int i = 0; i < attr.getLength(); i++) {
					Node nd = attr.item(i);
					if(nd.getNodeType() == Node.ELEMENT_NODE) {
						Element e = (Element) nd;
						/* For every wanted tag */
						for(int j = 0; j < children.size(); j++) {
							/* Set element id number if found and if not already set */
							if(e.getNodeName().equals(children.get(j))) {
								if(ids[j] == -1) {
									ids[j] = i;
								}
							}
						}
					}
				}
			}
			/* Loop a hundred times if the correct node is not found */
			loops += 1;
		} while(ids[0] == -1 && loops < 100);
		
		/* Create and append lists */
		for(int i = 0; i < nodes.getLength(); i++) {
			/* For every parent */
			Node node = nodes.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE) {
				/* Get children */
				Element e = (Element) node;
				NodeList attributes = e.getChildNodes();
				ArrayList<String> atrlist = new ArrayList<>(children.size());
				
				/* Get attribute text content by id number */
				for(int j = 0; j < children.size(); j++) {
					Node nd = attributes.item(ids[j]);
					Element el = (Element) nd;
					atrlist.add(el.getTextContent());
				}
				
				elems.add(atrlist);
			}
		}
		return elems;
	}
}
