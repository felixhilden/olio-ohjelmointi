package V9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class T2 extends Application{
	@FXML
	private ChoiceBox<String> dropdown = new ChoiceBox<>();
	@FXML
	private TextField date;
	@FXML
	private TextField start;
	@FXML
	private TextField end;
	@FXML
	private TextField name;
	@FXML
	private TextArea textarea;
	
	@SuppressWarnings("unused")
	private static TheaterList theaters;
	private static ObservableList<String> dropdownItems;
	
	public static void main(String[] args) throws IOException{
		loadTheaters();
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent layout = FXMLLoader.load(getClass().getResource("V9T2.fxml"));
		Scene scene = new Scene(layout);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	static private void loadTheaters() throws IOException {
		URL url = new URL("http://www.finnkino.fi/xml/TheatreAreas/");
		BufferedReader br = new BufferedReader(
				new InputStreamReader(url.openStream()));
		String content = "";
		String line = "";
		
		while((line = br.readLine()) != null) {
			content += line + '\n';
		}
		
		LocationParser lp = new LocationParser(content);
		theaters = new TheaterList(lp.names, lp.ids);
		dropdownItems = FXCollections.observableArrayList(lp.names);
	}
	
	@FXML
	private void initialize() {
		dropdown.setItems(dropdownItems);
	}
	
	public void searchByTheater(ActionEvent event) {
		System.out.println("Searching by theater name.");
	}
	
	public void searchByMovie(ActionEvent event) {
		System.out.println("Searching by movie name.");
	}
}
