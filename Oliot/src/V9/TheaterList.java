package V9;

import java.util.ArrayList;

public class TheaterList{
	private ArrayList<Theater> theaters;
	
	public TheaterList() {
		theaters = new ArrayList<>();
	}
	
	public TheaterList(ArrayList<String> locs, ArrayList<String> ids) {
		theaters = new ArrayList<>();
		if(locs.size() == ids.size()) {
			for(int i = 0; i < locs.size(); i++)
				theaters.add(new Theater(locs.get(i), ids.get(i)));
		} else {
			System.out.println("WARNING: Bad arrays passed to TheaterList!");
		}
	}
	
	public String getLocation(int id) {
		return theaters.get(id).location;
	}
	
	public String getID(int id) {
		return theaters.get(id).ID;
	}
	
	public int size() {
		return theaters.size();
	}
	
	public void add(String loc, String id) {
		theaters.add(new Theater(loc, id));
	}
}

class Theater {
	public String location;
	public String ID;
	
	public Theater(String loc, String id) {
		location = loc;
		ID = id;
	}
}
