package V9;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class LocationParser{
	private Document doc;
	public ArrayList<String> names = new ArrayList<>();
	public ArrayList<String> ids = new ArrayList<>();
	
	public LocationParser(String content) {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dBuilder = dbf.newDocumentBuilder();
			
			doc = dBuilder.parse(new InputSource(new StringReader(content)));
			doc.getDocumentElement().normalize();
			
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		
		NodeList nodes = doc.getElementsByTagName("TheatreArea");
		for(int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) node;
				NodeList attributes = e.getChildNodes();
				for(int j = 0; j < attributes.getLength(); j++) {
					Node item = attributes.item(j);
					if(item.getNodeType() == Node.ELEMENT_NODE) {
						Element elem = (Element) item;
						if(elem.getTagName().equals("ID"))
							ids.add(elem.getTextContent());
						if(elem.getTagName().equals("Name"))
							names.add(elem.getTextContent());
					}
				}
			}
		}
	}
}