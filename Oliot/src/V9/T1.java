package V9;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class T1 extends Application{
	@FXML
	private ChoiceBox<String> dropdown = new ChoiceBox<>();
	@FXML
	private TextField date;
	@FXML
	private TextField start;
	@FXML
	private TextField end;
	@FXML
	private TextField name;
	
	public static void main(String[] args){
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent layout = FXMLLoader.load(getClass().getResource("V9T1.fxml"));
		Scene scene = new Scene(layout);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	@FXML
	private void initialize() {
		//dropdown.setItems("");
	}
	
	public void searchByTheater(ActionEvent event) {
		System.out.println("Searching by theater name.");
	}
	
	public void searchByMovie(ActionEvent event) {
		System.out.println("Searching by movie name.");
	}
}
