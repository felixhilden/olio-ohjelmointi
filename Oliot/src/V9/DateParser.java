package V9;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class DateParser {
	public static String parse(String in, String ifmt, String ofmt) {
		LocalDateTime dt = LocalDateTime.parse(in, DateTimeFormatter.ofPattern(ifmt));
		return dt.format(DateTimeFormatter.ofPattern(ofmt));
	}
	
	public static boolean isValid(String s, String format) {
		boolean valid = false;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
	        Date date = dateFormat.parse(s);
	        String out = dateFormat.format(date);
	        valid = s.equals(out); 
		} catch (Exception ignore) {}
		
		return valid;
	}
	
	public static String HHmm(String in) {
		if(isValid(in, "HH"))
			in = in + ":00";
		
		if(!isValid(in, "HH:mm"))
			in = "";
		
		return in;
	}
	
	public static String ddMMyyyy(String in) {
		Calendar cal = Calendar.getInstance();
		
		if(isValid(in, "dd.MM.yyyy"))
			return in;
		else if(isValid(in, "dd-MM-yyyy"))
			return parse(in, "dd-MM-yyyy", "dd.MM.yyyy");
		else if(isValid(in, "dd.MM")) {
			int year = cal.get(Calendar.YEAR);
			return in + "." + year;
		} else if(isValid(in, "dd-MM")) {
			int year = cal.get(Calendar.YEAR);
			return parse(in, "dd-MM", "dd.MM") + "." + year;
		} else if(isValid(in, "dd")) {
			int m = cal.get(Calendar.MONTH);
			int year = cal.get(Calendar.YEAR);

			String mon = null;
			if(m < 10) {
				mon = "0" + Integer.toString(m);
			} else {
				mon = Integer.toString(m);
			}
			
			return in + "." + mon + "." + year;
		} else {
			int year = cal.get(Calendar.YEAR);
			int m = cal.get(Calendar.MONTH) + 1;
			int d = cal.get(Calendar.DATE);
			
			String mon = null;
			if(m < 10) {
				mon = "0" + Integer.toString(m);
			} else {
				mon = Integer.toString(m);
			}
			
			String day = null;
			if(d < 10) {
				day = "0" + Integer.toString(d);
			} else {
				day = Integer.toString(d);
			}
			
			return day + "." + mon + "." + year;
		}
	}
}
