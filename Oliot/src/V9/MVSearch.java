package V9;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class MVSearch {
	private Document doc;
	private ArrayList<String> shows = new ArrayList<>();
	
	public MVSearch(TheaterList t, String keyword) {
		for(int i = 0; i < t.size(); i++) {
			String ID = t.getID(i);

			String url = "http://www.finnkino.fi/xml/Schedule/?area="+ ID;
			URLReader urlr = new URLReader(url);
			String content = urlr.get();
			
			try {
				parse(content, keyword);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void parse(String content, String keyword) throws ParseException {		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dBuilder = dbf.newDocumentBuilder();
			
			doc = dBuilder.parse(new InputSource(new StringReader(content)));
			doc.getDocumentElement().normalize();
			
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}

		ArrayList<String> starts = new ArrayList<>();
		ArrayList<String> titles = new ArrayList<>();
		ArrayList<String> theaters = new ArrayList<>();
		
		/* For a show tag:
		 * attr 31 = title
		 * attr 5 = start
		 * attr 55 = theater name
		 */
		
		NodeList nodes = doc.getElementsByTagName("Show");
		for(int i = 0; i < nodes.getLength(); i++) {
			/* For every show tag */
			Node node = nodes.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) node;
				NodeList attributes = e.getChildNodes();
				
				Node start = attributes.item(5);
				Node title = attributes.item(31);
				Node theater = attributes.item(55);

				Element stelem = (Element) start;
				Element ttelem = (Element) title;
				Element thelem = (Element) theater;
				
				if(ttelem.getTextContent().contains(keyword)) {
					String s = stelem.getTextContent();
					starts.add(DateParser.parse(s, "yyyy-MM-dd'T'HH:mm:ss", "HH:mm"));
					titles.add(ttelem.getTextContent());
					theaters.add(thelem.getTextContent());
				}
			}
		}

		for(int i = 0; i < titles.size(); i++) {
			shows.add(titles.get(i) + ", " + theaters.get(i) + " klo " + starts.get(i));
		}

	}
	
	public ArrayList<String> get(){
		return shows;
	}
}
