package V9;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ShowParser {
	private Document doc;
	private ArrayList<String> movs = new ArrayList<>();
	
	public ShowParser(String content, String begin, String end) {
		try {
			parse(content, begin, end);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public ShowParser(String content) {
		try {
			parse(content, "", "");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	private void parse(String content, String begin, String end) throws ParseException {		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder dBuilder = dbf.newDocumentBuilder();
			
			doc = dBuilder.parse(new InputSource(new StringReader(content)));
			doc.getDocumentElement().normalize();
			
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}

		ArrayList<String> titles = new ArrayList<>();
		ArrayList<String> starts = new ArrayList<>();
		
		NodeList nodes = doc.getElementsByTagName("Show");
		for(int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if(node.getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) node;
				NodeList attributes = e.getChildNodes();
				for(int j = 0; j < attributes.getLength(); j++) {
					Node item = attributes.item(j);
					if(item.getNodeType() == Node.ELEMENT_NODE) {
						Element elem = (Element) item;
						if(elem.getTagName().equals("Title"))
							titles.add(elem.getTextContent());
						if(elem.getTagName().equals("dttmShowStart")) {
							String s = elem.getTextContent();
							starts.add(DateParser.parse(s, "yyyy-MM-dd'T'HH:mm:ss", "HH:mm"));
						}
					}
				}
			}
		}
		
		begin = DateParser.HHmm(begin);
		end = DateParser.HHmm(end);
		
		if(begin.isEmpty() && !end.isEmpty())
			begin = "00:00";
		if(!begin.isEmpty() && end.isEmpty())
			end = "24:00";
		
		if(begin.isEmpty() && end.isEmpty()) {
			for(int i = 0; i < titles.size(); i++) {
				movs.add(titles.get(i) + " klo " + starts.get(i));
			}
		} else {
			
			Date t1 = new SimpleDateFormat("HH:mm").parse(begin);
			Date t2 = new SimpleDateFormat("HH:mm").parse(end);
			Date t3 = null;
			
			Calendar c1 = Calendar.getInstance();
			Calendar c2 = Calendar.getInstance();
			Calendar c3 = Calendar.getInstance();
			
			c1.setTime(t1);
			c2.setTime(t2);
			
			for(int i = 0; i < titles.size(); i++) {
				t3 = new SimpleDateFormat("HH:mm").parse(starts.get(i));
				c3.setTime(t3);
				Date d = c3.getTime();
				if(d.after(c1.getTime()) && d.before(c2.getTime()))
					movs.add(titles.get(i) + " klo " + starts.get(i));
			}
		}	
	}
	
	public ArrayList<String> get(){
		return movs;
	}
}
