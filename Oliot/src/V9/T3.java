package V9;

import java.io.IOException;
import java.util.Calendar;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class T3 extends Application{
	@FXML
	private ChoiceBox<String> dropdown = new ChoiceBox<>();
	@FXML
	private TextField date;
	@FXML
	private TextField start;
	@FXML
	private TextField end;
	@FXML
	private TextField name;
	@FXML
	private ListView<String> listview = new ListView<>();
	
	private static TheaterList theaters;
	private static ObservableList<String> dropdownItems;
	
	public static void main(String[] args) {
		loadTheaters();
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) {
		Parent layout = null;
		try {
			layout = FXMLLoader.load(getClass().getResource("V9T3.fxml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene(layout);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	static private void loadTheaters() {
		URLReader urlr = new URLReader("http://www.finnkino.fi/xml/TheatreAreas/");
		String content = urlr.get();
		LocationParser lp = new LocationParser(content);
		theaters = new TheaterList(lp.names, lp.ids);
		dropdownItems = FXCollections.observableArrayList(lp.names);
	}

	@FXML
	private void initialize() {
		dropdown.setItems(dropdownItems);
	}
	
	public void searchByTheater(ActionEvent event) {
		int id = dropdown.getItems().indexOf(dropdown.getValue());
		String ID = theaters.getID(id);
		
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int mon = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DATE);
		
		String url = "http://www.finnkino.fi/xml/Schedule/?area="
				+ ID + "&dt=" + day + '.' + mon + '.' + year;
		URLReader urlr = new URLReader(url);
		String content = urlr.get();
		ShowParser s = new ShowParser(content);
		listview.setItems(FXCollections.observableArrayList(s.get()));
	}
	
	public void searchByMovie(ActionEvent event) {
		System.out.println("Searching by movie name.");
	}
}
