package V9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class URLReader {
	private URL url = null;
	private String content = "";
	
	public URLReader(String s) {
		try {
			url = new URL(s);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		BufferedReader br;
		try {
			br = new BufferedReader(
					new InputStreamReader(url.openStream()));
			String line = "";
			while((line = br.readLine()) != null) {
				content += line + '\n';
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String get() {
		return content;
	}
	
}
